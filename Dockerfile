FROM centos:7
RUN yum -y update && yum -y install wget sudo nano
RUN cd /opt && wget http://repo.mysql.com/mysql80-community-release-el8.rpm
RUN cd /opt && sudo rpm -Uvh mysql80-community-release-el8.rpm
RUN sudo yum -y install mysql-server
RUN sudo grep 'password' /var/log/mysqld.log
EXPOSE 3306

